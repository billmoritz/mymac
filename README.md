# mymac-cookbook

A cookbook I am playing with to setup my Macbook

## Supported Platforms

* Mac OS X

## Attributes

* node['mymac']['dmgs'] - Hash of packages and download locations in be installed
* node['mymac']['brews'] - Array of homebrew packages to install
* node['mymac']['casks'] - Array of homebrew casks to install
* node['mymac']['pips'] - Array of python pip packages to install

## Usage

### mymac::default

Include `mymac` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[mymac::default]"
  ]
}
```

## License and Authors

Author:: Bill Moritz (<billmoritz@gmail.com>)
