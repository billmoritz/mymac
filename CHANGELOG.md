# 0.2.0

Homebrew casks support
Python pip support

# 0.1.1

Homebrew remove aws-iam-tools - https://github.com/Homebrew/homebrew/issues/28930

# 0.1.0

Initial release of mymac
