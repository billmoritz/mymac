name 'mymac'
maintainer 'Bill Moritz'
maintainer_email 'billmoritz@gmail.com'
license 'Apache 2.0'
description 'Sets up mymac the way I like it'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.2.0'

supports 'mac_os_x'

depends 'dmg', '~> 2.2'
depends 'boot2docker'
depends 'homebrew'
depends 'python'
