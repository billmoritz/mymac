#
# Cookbook Name:: mymac
# Recipe:: default
#
# Copyright (C) 2015 Snap One, Inc
#
# All rights reserved - Do Not Redistribute
#

node['mymac']['dmgs'].each do |package, source|
  dmg_package package do
    source source
    type 'app'
  end
end

remote_file "#{Chef::Config[:file_cache_path]}/1Password-5.1.zip" do
  checksum 'cd47dcfc12af333e1b4b62a2431c7499635eab01d4c409d9c63baccdcededcee'
  source 'http://aws.cachefly.net/dist/1P/mac4/1Password-5.1.zip'
end

execute 'unzip 1password' do
  command "unzip #{Chef::Config[:file_cache_path]}/1Password-5.1.zip"
  cwd '/Applications'
  not_if { ::File.directory?('/Applications/1Password 5.app') }
end

include_recipe 'homebrew'

node['mymac']['casks'].each do |cask|
  homebrew_cask cask
end

node['mymac']['brews'].each do |package|
  homebrew_package package
end

node['mymac']['pips'].each do |pip|
  python_pip pip
end

link 'subl command' do
  to '/usr/local/bin/subl'
  target_file '/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl'
end
