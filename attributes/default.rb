default['mymac']['dmgs'] = {
  sourcetree: 'http://downloads.atlassian.com/software/sourcetree/SourceTree_2.0.5.2.dmg',
  boot2docker: 'https://github.com/boot2docker/osx-installer/releases/download/v1.5.0/Boot2Docker-1.5.0.pkg'
}
default['mymac']['brews'] = %w(
  auto-scaling aws-cfn-tools aws-elasticache aws-elasticbeanstalk aws-sns-cli
  bmon htop cloud-watch curl dos2unix ec2-ami-tools ec2-api-tools elb-tools gist
  htop-osx hub iozone jq mackup markdown multimarkdown mtr ncdu ngrep
  nmap percona-toolkit pv pwgen rds-command-line-tools s3cmd siege tmux tree
  watch wget wireshark
)
default['mymac']['casks'] = %w(
  sublime-text google-chrome cyberduck droplr caffeine sourcetree dropbox
  evernote gas-mask macvim skype launchcontrol virtualbox spotify hazel
)

default['mymac']['pips'] = %w(aws-cli)
